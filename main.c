#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <complex.h>
#include <time.h>
#include "PhaseVocoder.h"
#include "windows.h"
#include "unistd.h"
//Prueba git

int main(int argc, char *argv[])
{
  int i = 0, sizeSigIn = 2048;
  double sigIn [ sizeSigIn ],Hann[ sizeSigIn ],magnitude[ sizeSigIn ], phase[ sizeSigIn ], previousPhase[sizeSigIn];
  double sigOut[ sizeSigIn ];
  complex fourierTrans;
  clock_t start,end;
  double dif;
 
  while (i<sizeSigIn){
    sigIn[i] =sin(2 * M_PI * 1002 *  ((double)i / 44100));
    previousPhase[i]=0;
    sigOut[i]=0;
    i++;
  }  
  
  // Do some calculation.
  start=clock();
  FilterHann(Hann,sizeSigIn);
   
  PhaseVocoderAnalysis( sigIn, magnitude, phase, previousPhase, Hann, sizeSigIn); 
  
  PhaseVocoderResyntesis( sigOut, magnitude, phase, Hann, sizeSigIn, (int *) sizeSigIn);

  end=clock();
  dif = (double)(end-start)*(double)1000 / (double)CLOCKS_PER_SEC;
  printf ("Your calculations took %f miliseconds to run.\n",(double) dif );

  system("PAUSE");	
  return 0;
}

