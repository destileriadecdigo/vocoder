#ifndef PhaseVovoder_h_
#define PhaseVocoder_h_
/*************************************************************************************************/
/**
***@file PhaseVocoder.h
***@brief Header of a library for making vocoder procces.
***
***@author Paco el Pirata
***
***Comments: There is nothing to comment
**************************************************************************************************/
/*************************************************************************************************
** INCLUDES SECTION
***
**************************************************************************************************/
#include <stdio.h>
#include <stdbool.h>
#include "complex.h"
/*************************************************************************************************
*** DEFINITIONS SECTION
***
**************************************************************************************************/
/**
*** @brief Constant which determines the MAX_SIZE a buffer is able to have.
 **/
#define MAX_SIZE (4096)
/*************************************************************************************************
*** FUNCTIONS SECTION
***
**************************************************************************************************/

/**
*** @brief Function which calculate the addition between both arrays.
*** @param X  Additive 1.
*** @param Y  Additive 2.
*** @param ConstX  Value of the constant which multiplies X.
*** @param ConstY  Value of the constant which multiplies Y.
*** @param Z  Result of addition.
*** @param Size  size of the arrays.
*** @Coment Constants helps to modify addition in to a substraction.
 **/
void MathAddBuffers( double *X, double *Y, int ConstX, int ConstY, double *Z, int Size);

/**
*** @brief Function that copy a bufer into the other one.
*** @param X  Original Buffer which holds the original data.
*** @param Y  Copy Buffer which is going to hold the copied data.
*** @param Size  Size of the data to copy.
 **/
void MathCopyBuffer( double *Original, double *Copy, int Size);

/**
*** @brief Function which calculate the division between both arrays.
*** @param X  Values of the array which is divided.
*** @param Y  Values of the array which divides.
*** @param Z  Values of the array with the result of division.
*** @param Size  Size of the arrays.
 **/
void MathDivideBuffers( double *X, double *Y, double *Z, int Size);

/**
*** @brief Function which divide an array by a constant.
*** @param X  Values of array which is divided.
*** @param Y  Constant which divides.
*** @param Z  Values of the array with the result of division.
*** @param Size  Size of the arrays.
 **/
void MathDivideBufferByConstant(double *X, double Y, double *Z, int Size);
/**
*** @brief A function which transform any magnitude and phase number in to a complex number.
*** @param Magnitude  Array which holds magnitude information.
*** @param Phase  Array which holds phase information.
*** @param Complex  Array which holds the complex information from magnitude and phase.
*** @param Size  Size of arrays
 **/
void MathEulerFunction( double *Magnitude, double *Phase, complex *Complex, int Size);

/**
*** @brief A function which computes a Hann window.
*** @param Hann  Array which hold the window values.
*** @param Size  Size of the window we want to make.
 **/
void FilterHann( double *Hann, int Size);

/**
*** @brief A function which interpoles signal to make it longer or shorter.
*** @param BufferIn  Buffer which holds the original samples.
*** @param BufferOut  Buffer which holds the interpolated signal.
*** @param SizeIn  Size of the imput buffer.
*** @param SizeResult  Size of the result buffer.
*** @param CurrentHopSize  Size of the block which was processed.
*** @param UsualHopSize  Size of the normal block to be processed.
 **/
void FilterInterpolation( double *BufferIn, double *BufferResult, int SizeIn, int *SizeResult, int CurrentHopSize, int UsualHopSize);

/**
*** @brief A function wich transform any array of complex values into its magnitude and phase values.
*** @param Complex  Array which holds the complex information.
*** @param Magnitude  Array which holds the magnitude information.
*** @param Phase  Array which holds the phase information.
*** @param Size  Size of the arrays.
 **/
void MathMagnitudeAndPhase( complex *Complex, double *Magnitude, double *Phase, int Size);

/**
*** @brief A function which calculate the multiplication between both arrays.
*** @param X  Array of values multiplier 1.
*** @param Y  Array of values multiplier 2.
*** @param Z  Array which contains the result of multiplication.
*** @param Size  Size of the arrays.
 **/
void MathMultiplyBuffers( double *X, double *Y, double *Z, int Size);

/**
*** @brief A function which multiplies between an array by a constant.
*** @param X  Array of values to be multiplied.
*** @param Y  Constant.
*** @param Z  Array which contains the result of multiplication.
*** @param Size  Size of the array.
 **/
void MathMultiplyBufferByConstant(double *X, int Y, double *Z, int Size);

/**
*** @brief Split 0 to 2PI range along the positions in an array.
*** @param Omega  Array that holds Omega values.
*** @param Size  Size of the array.
*** @param Hopsize  Frequency
 **/
void MathOmega( double *Omega, int Size, int Hopsize);

/**
*** @brief A function for the frequency analysis of the signal.
*** @param Signal  Array which holds the signal buffer. 
*** @param Magnitude  Array which holds the magnitude values of the transform.
*** @param Phase  Array which holds the phase values of the transform.
*** @param Previous_Phase  Array which holds the magnitude values of the previous processed singnal.
*** @param Window  Array which holds the values of the analysis window.
*** @param Size  Size of the analisys signal.
 **/
void PhaseVocoderAnalysis( double *Signal, double *Magnitude, double *Phase, double *Previous_Phase, 
                           double *Window, int Size);

/**
*** @brief A function for the resyntesis of the signal values from the frecuency transform.
*** @param Signal  Array which holds the signal buffer. 
*** @param Magnitude  Array which holds the magnitude values of the transform.
*** @param Phase  Array which holds the phase values of the transform.
*** @param Window  Array which holds the values of the analysis window.
*** @param Size  Size of the arrays which hold the magnitude and phase values.
*** @param SizeSignal  Size of the resyntetize signal.
 **/
void PhaseVocoderResyntesis( double *Signal, double *Magnitude, double *Phase, double *Window, 
                             int Size,int *SizeSignal );

/**
*** @brief Function which make the different values from the array being between 0 and pi.
*** @param Buffer  Array which holds values IN/OUT.
*** @param Size  Size of the array.
 **/
 void MathPrincArg( double *Buffer, int Size);
                          
/**
*** @brief Function which modify the possitions in the array.
*** @param Buffer  Array which holds the values IN/OUT.
*** @param Size  Size of the array.
 **/
void MathShift( double *Buffer, int Size);
#endif
