
//Prueba git
/*************************************************************************************************/
/**
 *** @file PhaseVocoder.c
 *** @brief Implementation PhaseVocoder
 ***
 *** @date: 18.5.2014
 *** @author: Lot Carrera Otal
 ***
 *** Comments: CoUnit test file for the Threads abstraction layer
 ***
**************************************************************************************************/
/*************************************************************************************************/
/** INCLUDES SECTION
***
**************************************************************************************************/
#include "PhaseVocoder.h"
#include "math.h"

#include "complex.h"
#include "fftw3.h"
/*DEFINITION SECTION*/
#define DEBUG_OUTFILES_MODE
//Only debug mode
/* Here the files for debugging */
#ifdef DEBUG_OUTFILES_MODE
  char* FileIn = "SignalInPVDebug.txt";
  char* FileModule = "SignalModuleFourierTransformPVDebug.txt";
  char* FilePhase = "SignalPhaseFourierTransformPVDebug.txt";
  char* FileInverseTransform = "SignalInverseTransformPVDebug.txt";
  char* FileGrain = "SignalGrainPVDebug.txt";
  char* FileOut = "SignalOutPVDebug.txt";
#endif


/*************************************************************************************************/
/** IMPLEMENTATION SECTION
***
**************************************************************************************************/
/*
void PrintValuesInAFile()
*/

void PrintValuesInAFile(char* NameFile, double* ValuesArray, int SizeArray){

  FILE *FileManager = fopen(NameFile, "w");
  int i = 0;
  printf("Entro en %s \n",NameFile);
  if( FileManager )
      printf( "creado (ABIERTO)\n" );
   else
   {
      printf( "Error (NO ABIERTO)\n" );
   }
  fprintf(FileManager, " File Name: %s \n",NameFile);
  fprintf(FileManager, " From: PhaseVocoder lib debug mode \n");
  fprintf(FileManager, " By:  Lot Carrera Otal \n \n");

  while (i<SizeArray){
    fprintf(FileManager," %f \n",ValuesArray[i]);
    i++;
  }  
  //fclose(FileManager);
  if( !fclose(FileManager) )
      printf( "Fichero cerrado\n" );
  else
   {
      printf( "Error: fichero NO CERRADO\n" );
   }
}


/*
  Implementation function MathAddBuffers
*/
void MathAddBuffers( double *X, double *Y, int ConstX, int ConstY, double *Z, int Size )
{
  int i = 0;
  
  while (i < Size){
    Z[i] = ConstX * X[i] + ConstY * Y[i];
    i++;
  }    
}

/*
  Implementation function MathCopyBuffer
*/
void MathCopyBuffer(double *Original, double *Copy, int Size)
{
  int i = 0;
  
  while(i < Size){
    Copy[i] = Original[i];
    i++;
  }
}

/*
  Implementation function MathDivideBuffers
*/
void MathDivideBuffers(double *X, double *Y, double *Z, int Size)
{ 
  int i = 0;
  
  while (i < Size){
    Z[i] = X[i] / Y[i];
    i++;
  }              
}
/*
  Implementation function MathDivideBufferByConstant
*/
void MathDivideBufferByConstant(double *X, double Y, double *Z, int Size)
{ 
  int i = 0;
  
  while (i < Size){
    Z[i] = X[i] / Y;
    i++;
  }              
}
/*
  Implementation function MathEulerFunction
*/
void MathEulerFunction(double *Magnitude, double *Phase, complex *Complex, int Size)
{
  int i = 0;
  
  while (i < Size){
    Complex[i] = Magnitude[i] * (cos( Phase[i] ) + sin( Phase[i] ) * I);
    i++;
  }
}

/*
  Implementation function FilterHann
*/
void FilterHann( double *Hann , int Size){ 
  int i = 0;  
  
  while (i < Size){
    Hann[i] = 0.5 * (1 - cos((2 * M_PI * i) / Size) );
    i++;
  }
}

/*
  Implementation function FilterInterpolation
*/
void FilterInterpolation(double *BufferIn, double *BufferResult, int Size, int *SizeResult, int CurrentHopSize, int UsualHopSize)
{
  int i = 0;
  int j = 0;                            
  int lx   = floor( Size * CurrentHopSize / UsualHopSize );
  int ix; 
  int ix1;
  double x;
  double dx;
  double dx1;

  while (i < lx){
    x = 1 + ((float) i * ((float) Size / (float) lx));
    ix = floor( x );
    ix1 = ix + 1;
    dx = x - ix;
    dx1 = 1 - dx;
    BufferResult[i] = 0;
          
    if (ix < Size){
      BufferResult[i] += BufferIn[ix] * dx1;
    }
          
    if (ix1 < Size){
      BufferResult[i] += BufferIn[ix1] * dx;
    }
          
    i++;
  }                           
  *SizeResult = lx;
  
  #ifdef DEBUG_OUTFILES_MODE

    PrintValuesInAFile(FileOut, BufferResult, lx);

  #endif                                         
}

/*
  Implementation function MathMagnitudeAndPhase
*/
void MathMagnitudeAndPhase(complex *Complex, double *Magnitude, double *Phase, int Size)
{    
  int i = 0;

  while (i < Size){
    Magnitude[i] = cabs( Complex[i] );
    Phase[i] = carg( Complex[i] );
    i++;
  } 
}

/*
  Implementation function MathMultiplyBuffers
*/

void MathMultiplyBuffers (double *X, double *Y, double *Z, int Size)
{ 
  int i = 0;
  
  while (i < Size){
    Z[i] = X[i] * Y[i];
    i++;
  }              
}

/*
  Implementation function MathMultiplyBufferByConstant
*/

void MathMultiplyBufferByConstant(double *X, int Y, double *Z, int Size)
{ 
  int i = 0;
  
  while (i < Size){
    Z[i] = X[i] * Y;
    i++;
  }              
}


/*
  Implementation function MathOmega
*/
void MathOmega(double *Omega, int Size, int Hopsize)
{
  int i = 0;
  
  while (i < Size){
    Omega[i] = (2 * M_PI * Hopsize * i) / Size;
    i++;      
  }
}

/*
  Implementation function PhaseVocoderAnalysis
*/
void PhaseVocoderAnalysis(double *Signal, double *Magnitude, double *Phase, double *Previous_Phase, 
                          double *Window, int Size)
{
  double signal_to_process[Size];
  double delta[Size];
  double omega[Size];
  complex transform[Size];
  
  #ifdef DEBUG_OUTFILES_MODE
  
    PrintValuesInAFile( FileIn, Signal, Size);

  #endif
  //Windowing the signal
  MathMultiplyBuffers(Signal, Window, signal_to_process, Size);
  //Shifting the signal
  MathShift(signal_to_process, Size);
  //Fast Fourier Analysis
  fftw_plan plan;
  plan = fftw_plan_dft_r2c_1d(Size, signal_to_process, transform, FFTW_ESTIMATE);
  fftw_execute( plan );
  //Converting into Magnitude and Phase
  MathMagnitudeAndPhase(transform, Magnitude, Phase, Size);
  //Calculating the difference from the last frame.************

  #ifdef DEBUG_OUTFILES_MODE

    PrintValuesInAFile(FileModule, Magnitude, Size);
    PrintValuesInAFile(FilePhase, Phase, Size);

  #endif
}

/*
  Implementation function PhaseVocoderResyntesis
*/
void PhaseVocoderResyntesis(double *Signal, double *Magnitude, double *Phase, double *Window, 
                            int Size, int *SizeSignal )
{
  double signal_to_process[Size];
  complex transform[Size];
    
  
  MathEulerFunction(Magnitude, Phase, transform, Size);

  fftw_plan plan;
  plan = fftw_plan_dft_c2r_1d(Size, transform, signal_to_process, FFTW_ESTIMATE);
  fftw_execute( plan );

  MathDivideBufferByConstant(signal_to_process,Size,signal_to_process,Size);

  #ifdef DEBUG_OUTFILES_MODE

    PrintValuesInAFile(FileInverseTransform, signal_to_process, Size);

  #endif  

  MathShift(signal_to_process, Size);
 

  
  MathMultiplyBuffers(signal_to_process, Window, Signal, Size);
  
  #ifdef DEBUG_OUTFILES_MODE

    PrintValuesInAFile(FileGrain, Signal, Size);

  #endif


 } 


/*
  Implementation function MathPringCarg
*/
void MathPrincArg(double *Buffer, int Size)
{
  double val = 0;
  int i = 0;
  int j = 0;
  
  while (i < Size){
    val = (Buffer[i] / M_PI);
    j = floor( val );
    
    if( fmod( Buffer[i], M_PI ) != 0 ){
      
      if( fmod( abs( j ), 2 ) != 0 ){
        Buffer[i] = Buffer[i] - ( j + 1 ) * M_PI;
      }
      else{
        Buffer[i] = Buffer[i] - j * M_PI;
      }
      
    }
    else{
      Buffer[i] = fmod( Buffer[i] / M_PI, 2 ) * M_PI;
    }
    
    i++;
  }
}

/*
  Implementation function MathShift
*/
void MathShift(double *Buffer, int Size)
{
  int odd = 0;
  double odd_holder;
  double even_holder;
  int n=0;
  int i=0;
  
  if (Size % 2 != 0){
    n = ( Size - 1 ) / 2;
    odd_holder = Buffer[0];
    odd = 1;
  }
  else{
    n = Size / 2;
  }
  
  while ( i < n ){
    even_holder = Buffer[i+odd];
    Buffer[i] = Buffer[n+i+odd];
    Buffer[n+i+odd] = even_holder;
    i++;
  }
  
  if( odd ){
    Buffer[n] = odd_holder;
  }
}
